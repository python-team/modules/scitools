Source: scitools
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Johannes Ring <johannr@simula.no>
Build-Depends: debhelper (>= 9), python-all, python-matplotlib, dh-python
Standards-Version: 3.9.6
Vcs-Git: https://salsa.debian.org/python-team/packages/scitools.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/scitools
Homepage: http://scitools.googlecode.com

Package: python-scitools
Architecture: all
XB-Python-Version: ${python:Versions}
Depends: python-numpy, python-gnuplot, python-matplotlib,
 python-tk, ${python:Depends}, ${misc:Depends}
Recommends: python-scipy, python-scientific, imagemagick, python-vtk,
 python-pyx, python-pmw, blt
Suggests: netpbm, ffmpeg, octave
Description: Python library for scientific computing
 SciTools is a Python package containing lots of useful tools for scientific
 computing in Python. The package is built on top of other widely used
 packages such as NumPy, SciPy, ScientificPython, Gnuplot, etc.
 .
 SciTools also comes with a plotting interface called Easyviz, which is a
 unified interface to various packages for scientific visualization and
 plotting. Both curve plots and more advanced 2D/3D visualization of scalar
 and vector fields are supported. The Easyviz interface was designed with
 three ideas in mind: 1) a simple, Matlab-like syntax; 2) a unified interface
 to lots of visualization engines (backends): Gnuplot, Matplotlib, Grace,
 Veusz, Pmw.Blt.Graph, PyX, Matlab, VTK, VisIt, OpenDX; and 3) a minimalistic
 interface which offers only basic control of plots: curves, linestyles,
 legends, title, axis extent and names. More fine-tuning of plots can be done
 by adding backend-specific commands.
